package com.example.ae.pelindo_3.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ae.pelindo_3.DetailActivity;
import com.example.ae.pelindo_3.R;
import com.example.ae.pelindo_3.response.PelindoItem;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by ae on 09-Oct-18.
 */

public class AdapterPelindo extends RecyclerView.Adapter<AdapterPelindo.MyViewHolder> {
        // Buat Global variable untuk manampung context
        Context context;
        List<PelindoItem> profile;
public AdapterPelindo(Context context, List<PelindoItem> data_profile) {
        // Inisialisasi
        this.context = context;
        this.profile = data_profile;
        }

@Override
public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Layout inflater
        View view = LayoutInflater.from(context).inflate(R.layout.profile_item, parent, false);

        // Hubungkan dengan MyViewHolder
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
        }

@Override
public void onBindViewHolder(MyViewHolder holder, final int position) {
        // Set widget
        holder.tvTitle.setText(profile.get(position).getTitle());

// Dapatkan url gambar
final String urlGambarProfile = "http://192.168.43.4/pelindo/profile/" + profile.get(position).getFoto();

        Picasso.with(context).load(urlGambarProfile).into(holder.ivGambarProfile);

        // Event klik ketika item list nya di klik
        holder.itemView.setOnClickListener(new View.OnClickListener() {
@Override
public void onClick(View view) {
        // Mulai activity Detail
        Intent varIntent = new Intent(context, DetailActivity.class);
        // sisipkan data ke intent
        varIntent.putExtra("JDL_PROFILE", profile.get(position).getTitle());
        //varIntent.putExtra("TGL_BERITA", profile.get(position).getTanggalPosting());
        varIntent.putExtra("PNS_PROFILE", profile.get(position).getInstansi());
        varIntent.putExtra("FTO_PROFILE", urlGambarProfile);
        varIntent.putExtra("ISI_PROFILE", profile.get(position).getIsiText());

        // method startActivity cma bisa di pake di activity/fragment
        // jadi harus masuk ke context dulu
        context.startActivity(varIntent);
        }
        });
        }
// Menentukan Jumlah item yang tampil
@Override
public int getItemCount() {
        return profile.size();
        }

public class MyViewHolder extends RecyclerView.ViewHolder {
    // Deklarasi widget
    ImageView ivGambarProfile;
    TextView tvJudulProfile,tvTitle;
    public MyViewHolder(View itemView) {
        super(itemView);
        // inisialisasi widget
        ivGambarProfile = (ImageView) itemView.findViewById(R.id.ivPosterProfile);
        tvJudulProfile = (TextView) itemView.findViewById(R.id.tvKeteranganTitles);
        tvTitle = (TextView) itemView.findViewById(R.id.tvTitles);
    }
}
}
