package com.example.ae.pelindo_3.responseLayanan;

import com.google.gson.annotations.SerializedName;

public class LayananItem{

	@SerializedName("foto")
	private String foto;

	@SerializedName("isi_text")
	private String isiText;

	@SerializedName("id")
	private String id;

	@SerializedName("title")
	private String title;

	@SerializedName("instansi")
	private String instansi;

	public void setFoto(String foto){
		this.foto = foto;
	}

	public String getFoto(){
		return foto;
	}

	public void setIsiText(String isiText){
		this.isiText = isiText;
	}

	public String getIsiText(){
		return isiText;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setInstansi(String instansi){
		this.instansi = instansi;
	}

	public String getInstansi(){
		return instansi;
	}

	@Override
 	public String toString(){
		return 
			"LayananItem{" + 
			"foto = '" + foto + '\'' + 
			",isi_text = '" + isiText + '\'' + 
			",id = '" + id + '\'' + 
			",title = '" + title + '\'' + 
			",instansi = '" + instansi + '\'' + 
			"}";
		}
}