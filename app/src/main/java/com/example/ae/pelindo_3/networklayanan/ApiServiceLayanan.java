package com.example.ae.pelindo_3.networklayanan;

import com.example.ae.pelindo_3.responseLayanan.ResponseLayanan;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by ae on 09-Oct-18.
 */

public interface ApiServiceLayanan {

    @GET("layanan_tampil.php")
    Call<ResponseLayanan> request_show_all_layanan();

}
