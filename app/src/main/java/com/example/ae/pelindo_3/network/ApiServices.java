package com.example.ae.pelindo_3.network;

import com.example.ae.pelindo_3.response.ResponsePelindo;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by ae on 07-Oct-18.
 */

public interface ApiServices {

    @GET("profile_tampil.php")
    Call<ResponsePelindo> request_show_all_profile();
}
