package com.example.ae.pelindo_3.responseLayanan;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class ResponseLayanan{

	@SerializedName("layanan")
	private List<LayananItem> layanan;

	@SerializedName("status")
	private boolean status;

	public void setLayanan(List<LayananItem> layanan){
		this.layanan = layanan;
	}

	public List<LayananItem> getLayanan(){
		return layanan;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ResponseLayanan{" + 
			"layanan = '" + layanan + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}