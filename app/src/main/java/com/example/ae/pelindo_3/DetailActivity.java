package com.example.ae.pelindo_3;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {

    ImageView gambarprofile;
    TextView pembuat;
    WebView wvKontenProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        gambarprofile = (ImageView) findViewById(R.id.ivGambarProfile);
        pembuat = (TextView) findViewById(R.id.tvPenulisInstansi);
        wvKontenProfile = (WebView) findViewById(R.id.wvKontenProfile);

        showDetailProfile();

    }

    private void showDetailProfile() {
        String judul_profile = getIntent().getStringExtra("JDL_PROFILE");
        String pembuat_profile =  getIntent().getStringExtra("PNS_PROFILE");
        String isi_profile = getIntent().getStringExtra("ISI_PROFILE");
        String foto_profile = getIntent().getStringExtra("FTO_PROFILE");

        getSupportActionBar().setTitle(judul_profile);

        pembuat.setText(pembuat_profile);

        Picasso.with(this).load(foto_profile).into(gambarprofile);

        wvKontenProfile.getSettings().setJavaScriptEnabled(true);
        wvKontenProfile.loadData(isi_profile, "text/html; charset=utf-8", "UTF-8");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id==android.R.id.home){
            finish();
        }
        return true;
    }
}

