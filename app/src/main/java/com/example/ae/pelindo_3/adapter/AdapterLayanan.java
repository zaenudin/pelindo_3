package com.example.ae.pelindo_3.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ae.pelindo_3.DetailActivity;
import com.example.ae.pelindo_3.R;
import com.example.ae.pelindo_3.responseLayanan.LayananItem;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by ae on 09-Oct-18.
 */

public class AdapterLayanan extends RecyclerView.Adapter<AdapterLayanan.MyViewHolder>{

    Context context;
    List<LayananItem> layanan;

    public AdapterLayanan(Context context, List<LayananItem> data_layanan) {

    this.context = context;
    this.layanan = data_layanan;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.profile_item,parent,false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tvTitle.setText(layanan.get(position).getTitle());

        final String urlGambarProfile = "http://192.168.43.4/pelindo/layanan/" + layanan.get(position).getFoto();

        Picasso.with(context).load(urlGambarProfile).into(holder.ivGambarLayanan);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent varIntent = new Intent(context, DetailActivity.class);
                varIntent.putExtra("JUDUL_PROFILE",layanan.get(position).getTitle());
                varIntent.putExtra("PNS_PROFILE", layanan.get(position).getInstansi());
                varIntent.putExtra("FTO_PROFILE", urlGambarProfile);
                varIntent.putExtra("ISI_PROFILE", layanan.get(position).getIsiText());

                context.startActivity(varIntent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return layanan.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView ivGambarLayanan;
        TextView tvJudulProfile,tvTitle;

        public MyViewHolder(View itemView) {
            super(itemView);

            ivGambarLayanan = (ImageView) itemView.findViewById(R.id.ivPosterProfile);
            tvJudulProfile = (TextView) itemView.findViewById(R.id.tvKeteranganTitles);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitles);
        }
    }
}
