package com.example.ae.pelindo_3.response;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ResponsePelindo{

	@SerializedName("pelindo")
	private List<PelindoItem> pelindo;

	@SerializedName("status")
	private boolean status;

	public void setPelindo(List<PelindoItem> pelindo){
		this.pelindo = pelindo;
	}

	public List<PelindoItem> getPelindo(){
		return pelindo;
	}

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"ResponsePelindo{" + 
			"pelindo = '" + pelindo + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}