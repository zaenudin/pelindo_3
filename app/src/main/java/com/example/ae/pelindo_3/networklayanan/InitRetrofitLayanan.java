package com.example.ae.pelindo_3.networklayanan;



import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ae on 09-Oct-18.
 */

public class InitRetrofitLayanan {
    public static String API_URL = "http://192.168.43.4/pelindo/";

    public static Retrofit setInit() {
        return new Retrofit.Builder().baseUrl(API_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static ApiServiceLayanan getInstance() {
        return setInit().create(ApiServiceLayanan.class);
    }
}

