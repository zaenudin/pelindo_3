package com.example.ae.pelindo_3;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.Toast;

import com.example.ae.pelindo_3.adapter.AdapterLayanan;
import com.example.ae.pelindo_3.networklayanan.ApiServiceLayanan;
import com.example.ae.pelindo_3.networklayanan.InitRetrofitLayanan;
import com.example.ae.pelindo_3.responseLayanan.LayananItem;
import com.example.ae.pelindo_3.responseLayanan.ResponseLayanan;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LayananActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private RecyclerView recyclerView;

    private DrawerLayout drawerLayout;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_layanan);

        recyclerView = (RecyclerView) findViewById(R.id.rvListLayanan);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        getSupportActionBar().setTitle("Jasa Layanan");

       // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tampilLayanan();
        drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout_layanan);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.navigation_drawer_open,R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView)findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


    }

    private void tampilLayanan() {
        ApiServiceLayanan api = InitRetrofitLayanan.getInstance();
        Call<ResponseLayanan> layananCall = api.request_show_all_layanan();

        layananCall.enqueue(new Callback<ResponseLayanan>() {
            @Override
            public void onResponse(Call<ResponseLayanan> call, Response<ResponseLayanan> response) {

                if (response.isSuccessful()){
                    Log.d("response api",response.body().toString());
                    List<LayananItem> data_layanan = response.body().getLayanan();
                    boolean status = response.body().isStatus();
                    if (status) {
                        AdapterLayanan adapter = new AdapterLayanan(LayananActivity.this, data_layanan);

                        recyclerView.setAdapter(adapter);

                    }else{
                        Toast.makeText(LayananActivity.this,"",Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseLayanan> call, Throwable t) {

                t.printStackTrace();

            }
        });
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            startActivity(new Intent(LayananActivity.this,MainActivity.class));
        } else if (id == R.id.nav_layanan) {
            startActivity(new Intent(LayananActivity.this,LayananActivity.class));

        } else if (id == R.id.nav_tatakelola) {

        } else if (id == R.id.nav_infoinvestor) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id==android.R.id.content){

         }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout_layanan);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

}
